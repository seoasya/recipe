# -*- coding: utf-8 -*-

import scrapy
from allrecip2.items import Allrecip2Item
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.loader import ItemLoader
import sys
print sys.version_info
from scrapy.http import *
import json


class DmozSpider(CrawlSpider):
    name = "dmoz"
    allowed_domains = ["https://apps.allrecipes.com/v1/recipes/109587"]
    start_urls = [
        "https://apps.allrecipes.com/v1/recipes/109587",
    ]

    def start_requests(self):
        url = "https://apps.allrecipes.com/v1/recipes/"
        froms = ['109587']
        params = "?fields=nutrition&isMetric=false&servings=8"

        for x in froms:
            url_to_parse = '%s%s%s' % (url, x, params)
            yield Request(url_to_parse, callback=self.parse, method="GET",
                          headers={'Authorization': 'Bearer 84XAr4Z2Z57+ieH6qwuIrR1yFx7NDS4C09m1T91EDE5BG9MvXsAZCnYrRTVCoFSsWbc9xfziCS0aL1D2/26thUYixn5k9+Y+CFyYNOfiKesPL+X7NU9mocUrhMhmfpo1SLanUlBMzQvqqNhR25W7jwyclM1EYaFB5znwGoxcIOU1/g/NDigofg=='})


    def parse(self, response):
        # print x
        self.logger.info('Hi, this is a resp BODY %s', response.body)
        resp = response.body
        resp_parsed = json.loads(resp)
        self.logger.info('Hi, this is a resp JSON %s', resp_parsed)
        for nutrition in resp_parsed:
            item = ItemLoader(item=Allrecip2Item(), response=response)
            item.add_value('resp', resp_parsed['nutrition'])
            #item.add_value('direction', resp_parsed['nutrition']['calcium'])
            #item.add_value('from_direction', resp_parsed['response']['directions'][0]['from'])
            i = item.load_item()
            yield i
