import json
import re
import scrapy

from allrecip2.items import Allrecip2Item

is_empty = lambda x, y=None: x[0] if x else y


class AllrecipesItem(scrapy.Item):
    # define the fields for your item here like:
    name = scrapy.Field()
    servings = scrapy.Field()
    cook_time = scrapy.Field()
    prep_time = scrapy.Field()
    url = scrapy.Field()
    tags = scrapy.Field()
    ingredients = scrapy.Field()
    calculated_nutrition_info = scrapy.Field()
    instructions = scrapy.Field()


class AllrecipesProductsSpider(scrapy.Spider):
    name = 'allrecipes'
    allowed_domains = ["allrecipes.com"]
    base_url = 'http://allrecipes.com/recipes/1580/healthy-recipes/low-fat/main-dishes/'
    start_urls = [base_url]
    headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) '
                             'AppleWebKit/537.36 (KHTML, like Gecko) '
                             'Chrome/57.0.2987.133 Safari/537.36', }

    def __init__(self, *args, **kwargs):
        super(AllrecipesProductsSpider, self).__init__(site_name=self.allowed_domains[0], *args, **kwargs)
        self.current_page = 0
        self.result = {}
        self.page_count = 0

    def start_requests(self):
        yield scrapy.Request(url=self.start_urls[0], callback=self.parse_pages, headers=self.headers)
        # yield scrapy.Request(url=self.start_urls[0], callback=self.parse_links, headers=self.headers)

    def parse_pages(self, response):



        #this need to crawl first page

        if self.page_count == 0:
            # self.logger.info('it's here)
            request = scrapy.Request(url=self.base_url, callback=self.parse_links, dont_filter=True)
            yield request
        self.page_count = 1
        ###############################

        next_page = response.xpath("//link[@rel='next']/@href").extract()

        page_links = []

        if next_page:
            next_href = next_page[0]
            next_page_url = next_href
            page_links.append(next_page_url)
            request = scrapy.Request(url=next_page_url, callback=self.parse_pages, dont_filter=True)
            yield request

        for link in page_links:
            yield scrapy.Request(url=link, callback=self.parse_links, dont_filter=True)


        #
        # print("This is the next page " + next_page[0])
        #page_in_list = 'http://allrecipes.com/recipes/739/healthy-recipes/diabetic/'

        # for page_num in range(1, 3):
        #     next_page_link = self.base_url + '?page=' + str(page_num)
        #     page_links.append(next_page_link)
        #
        # for link in page_links:
        #     yield scrapy.Request(url=link, callback=self.parse_links, dont_filter=True)

    def parse_links(self, response):
        if response.body:
            href_links = list(
                set(response.xpath("//article[contains(@class, 'grid-col--fixed-tiles')]//a/@href").extract()))

            if href_links:
                for href in href_links:
                    if '/recipe/' in href:
                        url = "https://apps.allrecipes.com/v1/recipes/"
                        froms = href.split('/')[2]
                        params = "?fields=nutrition&isMetric=false&servings=8"
                        url_to_parse = '%s%s%s' % (url, froms, params)
                        href = 'http://allrecipes.com%s' % href
                        yield scrapy.Request(url_to_parse, callback=self.parse, meta={'froms': froms, 'href': href},
                                             method="GET",
                                             headers={
                                                 'Authorization': 'Bearer 84XAr4Z2Z57+ieH6qwuIrRB/DqQcE8OHG7FdlLzhJ1/pr+7Ckcky8Fcu2YOxCPrA4gYjLfT31R1JdIP93y3xNGwPXJ6mc53euiOlC09+AB2lkv2a/swtxeK/qVWaFgTvAJe0QIpnNsjeio+5CxjYOno9KmL4BQtluRspSuZjg6o='})

    def parse(self, response):
        froms = response.meta['froms']
        href = response.meta['href']

        resp = response.body
        resp_parsed = json.loads(resp.decode("utf-8"))

        prod = Allrecip2Item()
        prod['froms'] = response.meta['froms']
        prod['nutrition'] = resp_parsed['nutrition']

        yield scrapy.Request(url=href, callback=self.parse_product, headers=self.headers,
                             dont_filter=True, meta={'froms': froms, 'item': prod, 'url': href})

    def parse_product(self, response):
        product = AllrecipesItem()
        url = response.meta['url']
        nutrition = response.meta['item']['nutrition']

        # Parse review_count
        review_count = response.xpath(
            "//a[contains(@class, 'read--reviews')]//span[@class='review-count']/text()").extract()
        if review_count:
            review_count = int(re.search('\d+', review_count[0]).group())

        # Parse average_rating
        average_rating = response.xpath(
            "//section[contains(@class, 'recipe-summary')]//div[@class='rating-stars']/@data-ratingstars").extract()
        if average_rating:
            average_rating = float(average_rating[0])

        # Parse title
        title = response.xpath("//h1[@itemprop='name']/text()").extract()[0]

        # Parse ingredients
        ingredients = response.xpath(
            "//ul[contains(@class, 'list-ingredients')]//li//span[@itemprop='ingredients']/text()").extract()
        ingredients_count_num = 0
        if ingredients:
            ingredients_count_num = len(ingredients)

        # Parse categories
        categories = response.xpath(
            "//ul[contains(@class, 'breadcrumbs')]//li//span[@class='toggle-similar__title']/text()").extract()

        # Parse prep time
        prep_time_value = response.xpath("//ul[@class='prepTime']//li//time[@itemprop='prepTime']//span[@class='prepTime__item--time']/text()").extract()
        prep_time_unit = response.xpath("//ul[@class='prepTime']//li//time[@itemprop='prepTime']/text()").extract()

        try:
            prep_time = prep_time_value[0] + prep_time_unit[0]
        except IndexError:
            prep_time = ''

        # Parse cook time
        cook_time_value = response.xpath(
            "//ul[@class='prepTime']//li//time[@itemprop='cookTime']//span[@class='prepTime__item--time']/text()").extract()
        cook_time_unit = response.xpath("//ul[@class='prepTime']//li//time[@itemprop='cookTime']/text()").extract()

        try:
            cook_time = cook_time_value[0] + cook_time_unit[0]
        except IndexError:
            cook_time = ''

        # Parse number of servings
        servings = response.xpath("//meta[@id='metaRecipeServings']/@content").extract()
        servings = int(re.search('\d+', servings[0]).group())

        # Parse ready_in_time
        ready_in_time = response.xpath("//span[@class='ready-in-time']/text()").extract()
        ready_in_time_count = 0
        ready_in_time_number = 0
        if ready_in_time:
            ready_in_time_count = len(str(ready_in_time))
            ready_in_time_number = int(re.search('\d+', ready_in_time[0]).group())

        # Parse step
        step = response.xpath(
            "//li[contains(@class, 'step')]//span[@class='recipe-directions__list--item']/text()").extract()

        # get required nutrients
        has_complete_fat = nutrition['fat']['hasCompleteData']
        has_complete_sodium = nutrition['sodium']['hasCompleteData']
        has_complete_carbs = nutrition['carbohydrates']['hasCompleteData']
        has_complete_protein = nutrition['protein']['hasCompleteData']

        # parse calculated nutrition info
        calculated_nutrition_info = {
            "energy_kcal": nutrition['calories']['amount'] * servings,
            "total_fat": nutrition['fat']['amount'] * servings,
            "saturated_fat": nutrition['saturatedFat']['amount'] * servings,
            "cholesterol": nutrition['cholesterol']['amount'] * servings,
            "sodium": nutrition['sodium']['amount'] * servings,
            "total_carbohydrate": nutrition['carbohydrates']['amount'] * servings,
            "dietary_fiber": nutrition['fiber']['amount'] * servings,
            "sugars": nutrition['sugars']['amount'] * servings,
            "protein": nutrition['protein']['amount'] * servings,
            "potassium": nutrition['potassium']['amount'] * servings
        }

        # changed for tests

        if review_count >= 2 and average_rating >= 4 and ingredients_count_num <= 6 and ready_in_time_count == 8\
                and has_complete_fat and has_complete_sodium and has_complete_carbs and has_complete_protein:
            if ready_in_time_number < 60:
                product['name'] = title
                product['servings'] = servings
                product['cook_time'] = cook_time
                product['prep_time'] = prep_time
                product['url'] = url
                product['tags'] = categories
                product['ingredients'] = ingredients
                product['instructions'] = step
                product['calculated_nutrition_info'] = calculated_nutrition_info

                return product

    def _clean_text(self, text):
        text = text.replace("\n", " ").replace("\t", " ").replace("\r", " ")
        text = re.sub("&nbsp;", " ", text).strip()

        return re.sub(r'\s+', ' ', text)